###  U-SHORT

**U-SHORT** is a modern URL shortener with support for custom domains. Shorten URLs, manage your links and view the click rate statistics.

This project is used to make compact urls. It is built in php , on top of laravel.

#### Future plans

* captcha support
* duplicate url detection
* temporary urls
* secure redirect using cloudflare