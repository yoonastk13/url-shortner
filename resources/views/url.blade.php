@extends('layouts.master')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add URL
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" id="urladding">
            <!-- Small boxes (Stat box) -->
            <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-xs-6">
                    <div class="input-group margin">
                        <input type="text" class="form-control" id="urlvalue">
                        <span class="input-group-btn">
                      <button type="button" id="addUrl" class="btn btn-primary btn-flat">Add</button>
                    </span>
                    </div>
                </div>
            </div>
                <div class="row" id="urltable">
                    <div class="col-md-6">
                    <div class="box" style="margin-top: 50px;">
                        <div class="box-header">
                            <h3 class="box-title">Url Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tbody><tr>

                                    <th>Url</th>
                                    <th>Url</th>
                                </tr>
                                @foreach($url as $url)
                                <tr>


                                    <td>{{$url->url}}</td>
                                    <td><a href="/get-url/{{$url->shorten_url}}" target="_blank">{{ url('/') }}/get-url/{{$url->shorten_url}}</a> </td>



                                </tr>
                                @endforeach
                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    $(document).ready(function() {

        $("#addUrl").click(function () {
            var value=$("#urlvalue").val();
            if(value==''){

                alert ("Enter Valid Url")

            }else{

                var value=$("#urlvalue").val();
                var url ='/url-post';
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {'data': value},
                    success: function (data) {





                        $('#urladding #urltable').load(location.href + " #urltable");

                        $("#urlvalue").val('');
                        alert ("Added Succesfully");


                    }
                });

            }



        });
   $("#check").click(function () {

       alert ($(this).val());
            var value=$("this").val();
            if(value==''){

                alert ("Enter Valid Url")

            }else{

                var value=$("#urlvalue").val();
                var url ='/url-post';
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {'data': value},
                    success: function (data) {





                        $('#urladding #urltable').load(location.href + " #urltable");

                        $("#urlvalue").val('');
                        alert ("Added Succesfully");


                    }
                });

            }



        });
    });



</script>
@endsection